from imo import link
# from imo import dummy
from selenium.webdriver.common.by import By 
from selenium import webdriver
import time
import pandas as pd
import sys
import os
from selenium.webdriver.common.keys import Keys
driver = webdriver.Firefox()
# url='https://www.magicbricks.com/7-hills-narsingi-hyderabad-pdpid-4d4235303936383037'
longitude=[]
latitude=[]
T_units=[]
T_towers=[]
Address=[]
Pincode=[]
P_area=[]
name=[]
def getdata(url):
    driver.get(url)
    time.sleep(2)
    try:
        x=driver.find_elements_by_class_name("sub-wrap")
        y=x[0].find_elements_by_class_name("heading")[0].text
    
        name.append(y)
    except:
        name.append("Nan")
    
    z = driver.find_elements_by_xpath("//div[contains(@itemprop, 'geo')]")
    
    loc=z[0]
    try:
        latweb=loc.find_element_by_xpath("//meta[contains(@itemprop, 'latitude')]")
    
        latitude.append(latweb.get_attribute('content'))
    except:
        latitude.append("Nan")
    try:
        longweb=loc.find_element_by_xpath("//meta[contains(@itemprop, 'longitude')]")
        longitude.append(longweb.get_attribute('content'))
    except:
        longitude.append("Nan")
    # print(longitude)
    # print(latitude)
    # print("latitude is ",latweb.get_attribute('content'))
    # print("longitude is ",longweb.get_attribute('content'))
    # print(type(z))
    # print(len(z))
    curr_tunits="Nan"
    curr_ttowers="Nan"
    curr_faddress="Nan"
    curr_pincode="Nan"
    curr_parea="Nan"
    a=driver.find_elements_by_class_name("proj-info__data__block")
    time.sleep(0.5)
    for i in range(len(a)):
        b=a[i].find_element_by_class_name("proj-info__data__title").text
        time.sleep(0.1)
        if(b == 'Total Units'):
            try:
                curr_tunits=a[i].find_element_by_class_name("proj-info__data__value").text
            except:
                print('')
            
        if(b == 'Total Towers'):
            try:
                curr_ttowers=a[i].find_element_by_class_name("proj-info__data__value").text
                
            except:
                print('')
        
        if(b == 'Full Address'):
            try:
                curr_faddress=a[i].find_element_by_class_name("proj-info__data__value").text
                
            except:
                print('')
        if(b == 'Pincode'):
            try:
                curr_pincode=a[i].find_element_by_class_name("proj-info__data__value").text
                
            except:
                print('')
        if(b == 'Project Area'):
            try:
                curr_parea=a[i].find_element_by_class_name("proj-info__data__value").text
            except:
                print('')
    
    
    T_units.append(curr_tunits)
    T_towers.append(curr_ttowers)
    Address.append(curr_faddress)
    Pincode.append(curr_pincode)
    P_area.append(curr_parea)
# getdata(url)
cnt=1
for url in link:
    print(cnt)
    getdata(url)
    cnt+=1
# print(len(name))
# print(len(longitude))
# print(len(latitude))
# print(len(T_units))
# print(len(T_towers))
# print(len(Address))
# print(Pincode)
# print(P_area)   
data = list(zip(name,longitude,latitude,T_units,T_towers,Address,Pincode,P_area))
# print(data)
df = pd.DataFrame(data,columns=['name','longitude','latitude','T_units','T_towers','Address','Pincode','P_area'])
df.to_csv('inside2.csv')
